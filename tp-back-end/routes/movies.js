var express = require('express');

const axios = require('axios');

// Lodash utils library
const _ = require('lodash');
var router = express.Router();

let movies=[{
    id: "0",
    movie: "inception",
    yearOfRelease: "2010",
    duration: "148",
    actors: ["Leonardo DiCaprio", "Joseph Gordon-Levitt", "Ellen Page", "Tom Hardy","Plot"],
    poster: "https://m.media-amazon.com/images/M/MV5BMjAxMzY3NjcxNF5BMl5BanBnXkFtZTcwNTI5OTM0Mw@@._V1_SX300.jpg",
    boxOffice: "$292,568,851",
    rottenTomatoesScore: "86%"
}];

// Send a POST request
axios({
  method: 'post',
  url: 'http://localhost:3000/movies',
  data: {
    yearOfRelease: "2010",
    duration: "148",
    actors: ["Leonardo DiCaprio", "Joseph Gordon-Levitt", "Ellen Page", "Tom Hardy","Plot"],
    poster: "https://m.media-amazon.com/images/M/MV5BMjAxMzY3NjcxNF5BMl5BanBnXkFtZTcwNTI5OTM0Mw@@._V1_SX300.jpg",
    boxOffice: "$292,568,851",
    rottenTomatoesScore: "86%"

  }
});

axios('http://localhost:3000/movies');

/* GET movies listing. */
router.get('/', (req, res) => {
    // Get List of user and return JSON
    res.status(200).json({ movies });
  });

/* GET one movie. */
router.get('/:id', (req, res) => {
    const { id } = req.params;
    // Find user in DB
    const movie = _.find(movies, ["id", id]);
    // Return user
    res.status(200).json({
      message: 'Movie found!',
      movie 
    });
});
  
 
/* PUT new movie. */
router.put('/', (req, res) => {
    // Get the data from request from request
    const {movie} = req.body;
    // Create new unique id
    const id = _.uniqueId();
    // Insert it in array (normaly with connect the data with the database)
    movies.push({movie, id});
    // Return message
    res.json({
      message: `Just added ${id}`,
      movie: { movie, id}
    });
  });

  /* DELETE movie. */
router.delete('/:id', (req, res) => {
    // Get the :id of the movie we want to delete from the params of the request
    const {id} = req.params;
  
    // Remove from "DB"
    _.remove(movies, ["id", id]);
  
    // Return message
    res.json({
      message: `Just removed ${id}`
    });
  });

  /* UPDATE movie. */
router.post('/:id', (req, res) => {
  // Get the :id of the user we want to update from the params of the request
  const {id} = req.params;
  // Get the new moveie of the user we want to update from the body of the request
  const {movie} = req.body;
  // Find in DB
  const movieToUpdate = _.find(movies, ["id", id]);
  // Update data with new data (js is by address)
  movieToUpdate.movie=movie;

  // Return message
  res.json({
    message: `Just updated ${id} with ${movie}`
  });
});



module.exports = router;